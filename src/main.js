import Vue from 'vue';
import App from './App.vue';
import router from './router';
import VueJsonToTable from 'vue-json-to-table';
Vue.use(VueJsonToTable);

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount('#app');